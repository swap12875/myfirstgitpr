

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TC001 {

	public static void main(String[] args) throws Exception {
		startTest();
		// TODO Auto-generated method stub

	}


	private static void startTest() throws Exception {
		WebDriver driver;

		System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		// TODO Auto-generated method stub
		driver.get("https://opensource-demo.orangehrmlive.com/");
		driver.findElement(By.xpath("//div[@id='divUsername']/span")).click();
		driver.findElement(By.id("txtUsername")).clear();
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		driver.findElement(By.id("frmLogin")).submit();

//		assert (By.id("menu_dashboard_index").equals("Dashboard"));
//		System.out.println("Dashboard verified");// this is to assert that login succsesfull in DashBoard;
		driver.findElement(By.id("welcome")).click();
		driver.findElement(By.linkText("Logout")).click();
		String expectedDashboardValue = "Dashboard";
		WebElement actualDashboardValue = driver.findElement(By.id("menu_dashboard_index"));

		assertEquals(actualDashboardValue, expectedDashboardValue);
	}

	private static boolean assertEquals(WebElement actualDashboardValue, String expectedDashboardValue) {
		return (actualDashboardValue.equals(expectedDashboardValue));
		// TODO Auto-generated method stub

	}
}

